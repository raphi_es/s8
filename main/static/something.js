$(function(){
$.ajaxSetup({ cache: false });
$('#search').keyup(function(){
 $('#result').html('');
 var searchField = $('#search').val();
 var expression = new RegExp(searchField, "i");
 $.getJSON("cat.json", function(data) {
  $.each(data, function(key, value){
   if (value.text.search(expression) != -1)
   {
    $('#result').append('<li class="list-group-item link-class">'+value.text+'</li>');
   }
  });   
 });
});

$('#result').on('click', 'li', function() {
 var click_text = $(this).text().split('|');
 $('#search').val($.trim(click_text[0]));
 $("#result").html('');
});
});