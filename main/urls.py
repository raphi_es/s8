from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('test3', views.test3, name='test3'),
    path('data/',views.data_func, name='data')
]
